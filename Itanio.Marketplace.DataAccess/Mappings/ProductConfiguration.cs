﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using Itanio.Marketplace.Domain.Entities;

namespace Itanio.Marketplace.DataAccess.Mappings
{
    public class ProductConfiguration : EntityTypeConfiguration<Product>
    {
        public ProductConfiguration()
        {
            Property(x => x.CreationDate).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Computed);
        }
    }
}