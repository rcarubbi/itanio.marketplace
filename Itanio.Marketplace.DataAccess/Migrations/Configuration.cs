using System.Data.Entity.Migrations;

namespace Itanio.Marketplace.DataAccess.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<DbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            AutomaticMigrationDataLossAllowed = true;
        }

        protected override void Seed(DbContext context)
        {
        }
    }
}