using System.Data.Entity.Migrations;

namespace Itanio.Marketplace.DataAccess.Migrations
{
    public partial class add_timestamps_to_product : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Product", "Active", c => c.Boolean(false));
            AddColumn("dbo.Product", "CreationDate", c => c.DateTime(false, defaultValueSql: "GETDATE()"));
            AddColumn("dbo.Product", "UpdateDate", c => c.DateTime());
        }

        public override void Down()
        {
            DropColumn("dbo.Product", "UpdateDate");
            DropColumn("dbo.Product", "CreationDate");
            DropColumn("dbo.Product", "Active");
        }
    }
}