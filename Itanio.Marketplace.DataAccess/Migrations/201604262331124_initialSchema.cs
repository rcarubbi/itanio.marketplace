using System.Data.Entity.Migrations;

namespace Itanio.Marketplace.DataAccess.Migrations
{
    public partial class initialSchema : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                    "dbo.Product",
                    c => new
                    {
                        Id = c.Int(false, true),
                        Name = c.String(),
                        Description = c.String(),
                        MaxStallments = c.Int(false),
                        Ammount = c.Decimal(false, 18, 2),
                        UIOptions_StartButtonText = c.String(),
                        UIOptions_EndButtonText = c.String(),
                        UIOptions_HeaderText = c.String(),
                        UIOptions_CustomerDataRequired = c.Boolean(false),
                        PaymentMethods = c.Int(false)
                    })
                .PrimaryKey(t => t.Id);
        }

        public override void Down()
        {
            DropTable("dbo.Product");
        }
    }
}