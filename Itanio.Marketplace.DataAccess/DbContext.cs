﻿using System;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using Itanio.Marketplace.DataAccess.Mappings;
using Itanio.Marketplace.Domain;
using Itanio.Marketplace.Domain.Entities;
using Itanio.Marketplace.Domain.ValueObjects;

namespace Itanio.Marketplace.DataAccess
{
    public class DbContext : System.Data.Entity.DbContext, IDbContext
    {
        public DbContext()
            : base("name=ItanioMarketplace")
        {
        }

        public virtual IDbSet<Product> Products { get; set; }

        public new void SaveChanges()
        {
            var saveTime = DateTime.Now;
            foreach (var entry in ChangeTracker.Entries().Where(e => e.State == EntityState.Modified))
                entry.Property("UpdateDate").CurrentValue = saveTime;
            base.SaveChanges();
        }

        public new virtual IDbSet<TEntity> Set<TEntity>() where TEntity : class
        {
            return base.Set<TEntity>();
        }

        public void Update<T>(T oldEntity, T newEntity) where T : class
        {
            Entry(oldEntity).CurrentValues.SetValues(newEntity);
            Entry(oldEntity).State = EntityState.Modified;
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Configurations.Add(new ProductConfiguration());
            modelBuilder.ComplexType<UIOptions>();
        }
    }
}