﻿using System.Linq;

namespace Itanio.Marketplace.Domain.Repositories.PagedResultsGenericRepository
{
    public interface ISortCriteria<T>
    {
        SortDirection Direction { get; set; }

        IQueryable<T> ApplyOrdering(IQueryable<T> query, bool useThenBy);
    }
}