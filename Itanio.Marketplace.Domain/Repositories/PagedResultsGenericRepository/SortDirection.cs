﻿namespace Itanio.Marketplace.Domain.Repositories.PagedResultsGenericRepository
{
    public enum SortDirection
    {
        Ascending,
        Descending
    }
}