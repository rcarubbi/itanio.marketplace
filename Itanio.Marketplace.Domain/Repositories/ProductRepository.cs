﻿using Itanio.Marketplace.Domain.Entities;
using Itanio.Marketplace.Domain.Repositories.PagedResultsGenericRepository;

namespace Itanio.Marketplace.Domain.Repositories
{
    public class ProductRepository : GenericRepository<Product>
    {
        public ProductRepository(IDbContext context)
            : base(context)
        {
        }

        //public Charge FindByZones(int sourceCode, int targetCode)
        //{
        //    SearchQuery<Charge> query = new SearchQuery<Charge>();
        //    query.AddFilter(c => c.From.Code == sourceCode && c.To.Code == targetCode);

        //    return Search(query).Entities.FirstOrDefault();
        //}
    }
}