﻿namespace Itanio.Marketplace.Domain.ValueObjects
{
    public class UIOptions
    {
        public string StartButtonText { get; set; }

        public string EndButtonText { get; set; }

        public string HeaderText { get; set; }

        public bool CustomerDataRequired { get; set; }
    }
}