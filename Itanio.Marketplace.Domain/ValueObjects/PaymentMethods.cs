﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Itanio.Marketplace.Domain.ValueObjects
{
    [Flags]
    public enum PaymentMethods
    {
        [Display(AutoGenerateField = false)] None = 0,

        [Display(Name = "Boleto")] [Description("boleto")]
        Boleto = 1 << 1,

        [Display(Name = "Cartão de Crédito")] [Description("credit_card")]
        CreditCard = 1 << 2
    }
}