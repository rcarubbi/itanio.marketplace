﻿using System;
using Itanio.Marketplace.Domain.ValueObjects;

namespace Itanio.Marketplace.Domain.Entities
{
    public class Product
    {
        public Product()
        {
        }

        public Product(int id)
        {
            Id = id;
        }


        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public int MaxStallments { get; set; }

        public decimal Ammount { get; set; }

        public virtual UIOptions UIOptions { get; set; }

        public PaymentMethods PaymentMethods { get; set; }

        public bool Active { get; set; }

        public DateTime CreationDate { get; set; }


        public DateTime? UpdateDate { get; set; }
    }
}