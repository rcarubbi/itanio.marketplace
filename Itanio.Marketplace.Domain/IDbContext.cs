﻿using System.Data.Entity;
using Itanio.Marketplace.Domain.Entities;

namespace Itanio.Marketplace.Domain
{
    public interface IDbContext
    {
        IDbSet<Product> Products { get; set; }
        void SaveChanges();

        void Update<T>(T oldValue, T newValue) where T : class;

        IDbSet<TEntity> Set<TEntity>() where TEntity : class;
    }
}