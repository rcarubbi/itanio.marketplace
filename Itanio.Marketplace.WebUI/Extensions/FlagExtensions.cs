﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace Itanio.Marketplace.WebUI.Extensions
{
    public static class FlagExtensions
    {
        public static string Description(this Enum value)
        {
            var type = value.GetType();

            var res = new List<string>();
            var arrValue = value.ToString().Split(',').Select(v => v.Trim());
            foreach (var strValue in arrValue)
            {
                var memberInfo = type.GetMember(strValue);
                if (memberInfo != null && memberInfo.Length > 0)
                {
                    var attrs = memberInfo[0].GetCustomAttributes(typeof(DescriptionAttribute), false);

                    if (attrs != null && attrs.Length > 0 &&
                        attrs.Where(t => t.GetType() == typeof(DescriptionAttribute)).FirstOrDefault() != null)
                        res.Add(((DescriptionAttribute) attrs.Where(t => t.GetType() == typeof(DescriptionAttribute))
                            .FirstOrDefault()).Description);
                    else
                        res.Add(strValue);
                }
                else
                {
                    res.Add(strValue);
                }
            }

            return res.Aggregate((s, v) => s + ", " + v);
        }
    }
}