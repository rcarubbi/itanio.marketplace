﻿using System;
using System.Web.Mvc;

namespace Itanio.Marketplace.WebUI
{
    internal class FlagsModelBinder<T> : DefaultModelBinder where T : struct
    {
        public override object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            var checkType = default(T);
            if (!(checkType is Enum)) throw new InvalidOperationException("Struct is not an Enum.");

            if (checkType.GetType().GetCustomAttributes(typeof(FlagsAttribute), false).Length == 0)
                throw new InvalidOperationException("Enum must use [Flags].");

            var value = bindingContext.ValueProvider.GetValue(bindingContext.ModelName);
            if (value != null)
            {
                var rawValues = value.RawValue as string[];
                if (rawValues != null)
                {
                    T result;
                    if (Enum.TryParse(string.Join(",", rawValues), out result)) return result;
                }
            }

            return base.BindModel(controllerContext, bindingContext);
        }
    }
}