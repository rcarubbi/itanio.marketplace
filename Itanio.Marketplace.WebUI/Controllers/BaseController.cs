﻿using System.IO;
using System.Web.Mvc;
using Itanio.Marketplace.Domain;

namespace Itanio.Marketplace.WebUI.Controllers
{
    public abstract class BaseController : Controller
    {
        protected IDbContext _context;

        public BaseController(IDbContext context)
        {
            _context = context;
        }

        protected string RenderRazorViewToString(string viewName, object model)
        {
            ViewData.Model = model;
            using (var sw = new StringWriter())
            {
                var viewResult = ViewEngines.Engines.FindPartialView(ControllerContext,
                    viewName);
                var viewContext = new ViewContext(ControllerContext, viewResult.View,
                    ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);
                viewResult.ViewEngine.ReleaseView(ControllerContext, viewResult.View);
                return sw.GetStringBuilder().ToString();
            }
        }
    }
}