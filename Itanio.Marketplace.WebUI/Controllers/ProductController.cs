﻿using System.Web.Mvc;
using Carubbi.Datatables;
using DataTables.Mvc;
using Itanio.Marketplace.Domain;
using Itanio.Marketplace.Domain.Entities;
using Itanio.Marketplace.Domain.Repositories.PagedResultsGenericRepository;
using Itanio.Marketplace.Domain.ValueObjects;
using Itanio.Marketplace.WebUI.Models;

namespace Itanio.Marketplace.WebUI.Controllers
{
    public class ProductController : BaseController
    {
        public ProductController(IDbContext context)
            : base(context)
        {
        }

        // GET: Charge
        public ActionResult Index()
        {
            ViewBag.Title = "Produtos";
            ViewBag.SubTitle = "Manutenção de Produtos";
            return View();
        }

        public ActionResult Edit(int? id)
        {
            var viewModel = new ProductViewModel();

            if (id.HasValue)
            {
                var product = _context.Products.Find(id);
                viewModel = ProductViewModel.FromEntity(product);
            }

            var modalViewModel = new ModalFormViewModel
            {
                Id = "EditProductModalForm",
                PartialViewName = "_EditProduct",
                Title = string.Format("{0} de Produto", id.HasValue ? "Alteração" : "Inclusão"),
                ViewModel = viewModel,
                Size = ModalSize.Medium
            };
            return PartialView("_ModalFormDialog", modalViewModel);
        }

        [HttpPost]
        public ActionResult Edit(ProductViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                if (viewModel.Id.HasValue)
                {
                    var currentProduct = _context.Products.Find(viewModel.Id);
                    var updatedProduct = ProductViewModel.ToEntity(viewModel, currentProduct.UIOptions);

                    _context.Update(currentProduct, updatedProduct);
                    _context.SaveChanges();
                }
                else
                {
                    var postedProduct = ProductViewModel.ToEntity(viewModel, new UIOptions());

                    _context.Products.Add(postedProduct);
                    _context.SaveChanges();
                }

                return PartialView("_productGrid");
            }

            return PartialView("_validationSummary", viewModel);
        }

        public ActionResult Delete(int id)
        {
            var viewModel = new ProductViewModel();

            var product = _context.Products.Find(id);

            viewModel = ProductViewModel.FromEntity(product);

            var modalViewModel = new ModalQuestionViewModel
            {
                Id = "DeleteProductModal",
                Title = "Exclusão de Produto",
                Body = string.Format("Tem certeza que deseja excluir o produto {0}?", viewModel.Name),
                YesButtonAction = Url.Action("Delete", "Product"),
                CloseNoButton = true,
                KeyProperties = string.Format("Id={0}", id),
                YesButtonUpdateContainerId = "product-grid-container",
                YesButtonNotification = new NotificationViewModel
                {
                    Type = NotificationType.Error,
                    Message = "Produto excluído com sucesso!",
                    Title = "Exclusão de Produto"
                }
            };
            return PartialView("_ModalQuestionDialog", modalViewModel);
        }

        [HttpPost]
        public ActionResult Delete(ProductViewModel viewModel)
        {
            var product = _context.Products.Find(viewModel.Id.Value);

            _context.Products.Remove(product);
            _context.SaveChanges();

            return PartialView("_productGrid");
        }

        public JsonResult Products([ModelBinder(typeof(DataTablesBinder))]
            IDataTablesRequest requestModel)
        {
            var repo = new GenericRepository<Product>(_context);
            var query = new SearchQuery<Product>();

            var sortedColumns = requestModel
                .Columns
                .GetSortedColumns()
                .ToDynamicExpression<ProductViewModel>();

            if (sortedColumns.Length > 0) query.AddSortCriteria(new DynamicFieldSortCriteria<Product>(sortedColumns));

            query.Take = requestModel.Length;
            query.Skip = requestModel.Start;

            var result = repo.Search(query);

            return Json(new DataTablesResponse(requestModel.Draw,
                ProductViewModel.FromEntityCollection(result.Entities), result.Count, result.Count));
        }
    }
}