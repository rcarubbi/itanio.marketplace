﻿using System.Net.Mail;
using System.Web.Mvc;
using Itanio.Marketplace.Domain;
using Itanio.Marketplace.WebUI.Models;
using PagarMe;

namespace Itanio.Marketplace.WebUI.Controllers
{
    public class SaleController : BaseController
    {
        public SaleController(IDbContext context)
            : base(context)
        {
        }

        // GET: Sale
        public ActionResult Index(int id)
        {
            var product = _context.Products.Find(id);
            return View(ProductViewModel.FromEntity(product));
        }

        [HttpPost]
        public ActionResult Buy(BuyViewModel viewModel)
        {
            PagarMeService.DefaultApiKey = "ak_test_lgKqOTGfDasjTSNYfXvrzMuaQWwmGE";
            var transaction = PagarMeService.GetDefaultService().Transactions.Find(viewModel.Token);
            transaction.Capture(transaction.Amount);
            TempData["transaction"] = transaction;
            return RedirectToAction(transaction.Status.ToString());
        }

        public ActionResult Paid()
        {
            var transaction = (Transaction) TempData["transaction"];
            //SendEmail(transaction);
            return View();
        }

        public ActionResult WaitingPayment()
        {
            var transaction = (Transaction) TempData["transaction"];
            //SendEmail(transaction);
            return Redirect(transaction.BoletoUrl);
        }

        public ActionResult Refused()
        {
            var transaction = (Transaction) TempData["transaction"];
            //SendEmail(transaction);
            return View();
        }

        private void SendEmail(Transaction transaction)
        {
            var smtpClient = new SmtpClient();
            var message = new MailMessage();

            message.Body = RenderRazorViewToString($"Email{transaction.Status}", transaction);
            message.To.Add(transaction.Customer.Email);
            message.From = new MailAddress("vendas@itanio.com.br");
            message.IsBodyHtml = true;
            message.Priority = MailPriority.Normal;
            smtpClient.Send(message);
        }
    }
}