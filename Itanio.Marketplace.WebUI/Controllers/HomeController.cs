﻿using System.Web.Mvc;

namespace Itanio.Marketplace.WebUI.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = "Home";
            ViewBag.SubTitle = "Portal de Produtos";
            return View();
        }
    }
}