﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Carubbi.Datatables;
using Itanio.Marketplace.Domain.Entities;
using Itanio.Marketplace.Domain.ValueObjects;

namespace Itanio.Marketplace.WebUI.Models
{
    public class ProductViewModel
    {
        [DataTablesColumn(PrimaryKey = true, Hidden = true)]
        public int? Id { get; set; }

        [DataTablesColumn(Order = 1, Header = "Nome")]
        [Display(Name = "Nome")]
        public string Name { get; set; }

        [Display(Name = "Descrição")]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }

        [DataTablesColumn(Order = 2, Header = "Valor", DataType = DataType.Currency)]
        [Display(Name = "Valor")]
        [DataType(DataType.Currency, ErrorMessage = "Digite um valor monetário válido")]
        public decimal Ammount { get; set; }

        [Display(Name = "Formas de pagamento")]
        public PaymentMethods PaymentMethods { get; set; }

        [DataTablesColumn(Order = 3, Header = "Número máx. de Parcelas")]
        [Display(Name = "Número máx. de Parcelas")]
        [Range(1, 10, ErrorMessage = "Escolha um número entre 1 e 10")]
        public int MaxStallments { get; set; }

        [Display(Name = "Solicitar os Dados do cliente?")]
        public bool CustomerDataRequired { get; set; }

        [Display(Name = "Texto do botão de conclusão")]
        public string EndButtonText { get; set; }

        [Display(Name = "Texto do cabeçalho")]
        [DataType(DataType.MultilineText)]
        public string HeaderText { get; set; }

        [Display(Name = "Texto do botão de início")]
        public string StartButtonText { get; set; }

        [Display(Name = "Ativo")] public bool Active { get; set; }

        [DataTablesColumn(Order = 4, Header = "Ativo")]
        public string ActiveDescription => Active ? "Sim" : "Não";

        internal static ProductViewModel FromEntity(Product product)
        {
            return new ProductViewModel
            {
                Name = product.Name,
                Description = product.Description,
                Ammount = product.Ammount,
                PaymentMethods = product.PaymentMethods,
                Id = product.Id,
                MaxStallments = product.MaxStallments,
                CustomerDataRequired = product.UIOptions.CustomerDataRequired,
                EndButtonText = product.UIOptions.EndButtonText,
                HeaderText = product.UIOptions.HeaderText,
                StartButtonText = product.UIOptions.StartButtonText,
                Active = product.Active
            };
        }

        internal static Product ToEntity(ProductViewModel viewModel, UIOptions uiOptions)
        {
            var product = new Product(viewModel.Id ?? 0)
            {
                Description = viewModel.Description,
                Ammount = viewModel.Ammount,
                Name = viewModel.Name,
                PaymentMethods = viewModel.PaymentMethods,
                MaxStallments = viewModel.MaxStallments,
                Active = viewModel.Active,
                UIOptions = uiOptions
            };
            product.UIOptions.CustomerDataRequired = viewModel.CustomerDataRequired;
            product.UIOptions.EndButtonText = viewModel.EndButtonText;
            product.UIOptions.StartButtonText = viewModel.StartButtonText;
            product.UIOptions.HeaderText = viewModel.HeaderText;

            return product;
        }


        internal static IEnumerable<ProductViewModel> FromEntityCollection(IEnumerable<Product> entities)
        {
            foreach (var item in entities) yield return FromEntity(item);
        }
    }
}