﻿namespace Itanio.Marketplace.WebUI.Models
{
    public enum ModalSize
    {
        ExtraSmall,
        Small,
        Medium,
        Large,
        ExtraLarge
    }
}